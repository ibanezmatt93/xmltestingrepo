﻿cd $PSScriptRoot

cd '..\'
cd '..\'

Compress-Archive -Path '.\Mary.docx_\*' -Update -DestinationPath '.\Mary.zip'

if (Test-Path '.\Mary.docx') {
    Remove-Item '.\Mary.docx'
}

Move-Item '.\Mary.zip' '.\Mary.docx'