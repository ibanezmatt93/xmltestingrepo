﻿cd $PSScriptRoot

Add-Type -A System.IO.Compression.FileSystem

cd '..\'
cd '..\'

if (Test-Path '.\Mary.docx_') {
    Remove-Item '.\Mary.docx_' -Recurse
}

Move-Item '.\Mary.docx' '.\Mary.zip'

Expand-Archive -Path '.\Mary.zip' -DestinationPath '.\Mary.docx_'

Move-Item '.\Mary.zip' '.\Mary.docx'

git add '..\Mary.docx_'