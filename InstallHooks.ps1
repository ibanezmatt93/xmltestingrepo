﻿cd $PSScriptRoot

## Hook Files
$preCommitHook = '.\Hooks\pre-commit.sample'
$postMergeHook = '.\Hooks\post-merge.sample'
$hooksFolderPreCommit = '.\.git\hooks\pre-commit'
$hooksFolderPostMerge = '.\.git\hooks\post-merge'

## PowerShell Scripts
$preCommitScript = '.\Hooks\UnzipDoc.ps1'
$hooksFolderPreCommitScript = '.\.git\hooks\UnzipDoc.ps1'
$postMergeScript = '.\Hooks\CompressDirectories.ps1'
$hooksFolderPostMergeScript = '.\.git\hooks\CompressDirectories.ps1'


if (-not (Test-Path $hooksFolderPreCommit)) {
    Move-Item $preCommitHook $hooksFolderPreCommit
}
if (-not (Test-Path $hooksFolderPostMerge)) {
    Move-Item $postMergeHook $hooksFolderPostMerge
}
if (-not (Test-Path $hooksFolderPreCommitScript)) {
    Move-Item $preCommitScript $hooksFolderPreCommitScript
}
if (-not (Test-Path $hooksFolderPostMergeScript)) {
    Move-Item $postMergeScript $hooksFolderPostMergeScript
}